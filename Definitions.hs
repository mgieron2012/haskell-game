module Definitions where
import           Data.List.Split

data Place = BotanicComplex | ExitCorridor | Hall | PlayerRoom | Cell1 | Cell2 | Cell3 | Cell4 | JailWing
data Item = Glasses | Extinguisher | Key | SpecimenX | Note | BunchOfKeys | Johnson deriving (Eq)
data Direction = N | E | S | W deriving (Eq, Read)
type Equipment = [Item]
data GameState = GameState {place :: Place, eq :: Equipment, guardsDefeated :: Bool}

-- print strings from list in separate lines
printLines :: [String] -> IO ()
printLines xs = putStr (unlines xs)

maybePrintLines :: Maybe [String] -> IO ()
maybePrintLines Nothing   = return ()
maybePrintLines (Just xs) = printLines xs

readCommand :: IO [Char]
readCommand = do
    putStr "> "
    getLine

readAndSplitCommand :: IO [String]
readAndSplitCommand = do
    putStr "> "
    xs <- getLine
    let splitted = splitOn " " xs
    return splitted
