module Main where
import           Definitions
import           Places
import           Text.Read   (readMaybe)

-- for coloured text:
-- red     bold    text reset
-- \ESC[31m\ESC[1m<text>\ESC[0m
-- < and > are not part of the string!

introductionText = [
    "Budzisz się w swojej celi - czujesz, że coś jest nie tak.",
    "Środek nocy, a mimo to nie towarzyszy Ci ten zbyt dobrze znany szum...",
    "3 miesiące i 2 dni - tyle już cię tu trzymają!",
    "...a to oznacza, że już czas! Zamek elektroniczny puścił, pora się stąd wydostać!",
    ""
    ]

instructionsText = [
    "Available commands are:",
    "",
    "where_am_i      -- to show your position description",
    "look_around     -- to look around",
    "go N/E/S/W      -- to go in the choosen direction",
    "enter cela_N    -- to enter the given cell",
    "equipment       -- to list all your equipment",
    "take itemName   -- to take an item into your equipment",
    "read            -- to read a note from your equipment",
    "release         -- to free someone from their cell",
    "attack          -- to attack prison guards",
    "escape          -- to escape from the facility",
    "instructions    -- to show instructions once again",
    "quit            -- to end the game and quit",
    ""
    ]

printIntroduction = printLines introductionText
printInstructions = printLines instructionsText

gameLoop state = do
    cmd <- readAndSplitCommand
    case cmd of
        ["instructions"] -> do printInstructions
                               gameLoop state
        ["where_am_i"] -> do placeInfo (place state)
                             gameLoop state
        ["look_around"] -> do describePlace (place state) (eq state)
                              gameLoop state
        ["take", itemName] -> do
            printLines message
            gameLoop nextState
            where
                (newEquipment, message) = takeItem (place state) (eq state) itemName
                nextState = GameState (place state) newEquipment (guardsDefeated state)
        ["go", direction] -> do
            maybePrintLines message
            placeInfo newPlace
            gameLoop nextState
            where
                (newPlace, message) = go (place state) (eq state) (readMaybe direction)
                nextState = GameState newPlace (eq state) (guardsDefeated state)
        ["enter", cellNumber] -> do
            maybePrintLines message
            placeInfo newPlace
            gameLoop nextState
            where
                (newPlace, message) = enter (place state) (eq state) cellNumber
                nextState = GameState newPlace (eq state) (guardsDefeated state)
        ["equipment"] -> do
            printEquipment (eq state)
            gameLoop state
        ["read"] -> do
            readNote (eq state)
            gameLoop state
        ["release"] -> do
            printLines message
            gameLoop nextState
            where
                (newEquipment, message) = release (place state) (eq state)
                nextState = GameState (place state) newEquipment (guardsDefeated state)
        ["attack"] -> do
            printLines message
            if died
                then printLines ["Twoje plany ucieczki spełzły na niczym.", ""]
            else gameLoop nextState
            where
                (newGuardsDefeated, died, message) = attack (place state) (eq state)
                nextState = GameState (place state) (eq state) newGuardsDefeated
        ["escape"] -> do
            printLines (escape (place state) (eq state) (guardsDefeated state))
        ["quit"] -> return ()
        _ -> do printLines ["Nieznana komenda.", ""]
                gameLoop state

start = do
    printInstructions
    printIntroduction
    describePlace PlayerRoom []
    gameLoop state where
        state = GameState PlayerRoom [] False

main = do
    start
