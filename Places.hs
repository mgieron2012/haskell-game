module Places where
import           Definitions


placeInfo :: Place -> IO ()
placeInfo Hall = printLines [
    "Stoisz w małym przedsionku łączącym wszystkie części ośrodka.",
    "Podejmij decyzję, gdzie się dalej udać - lepiej się nie pomyl!",
    ""]

placeInfo BotanicComplex = printLines [
    "Kompleks botaniczny - to tutaj trzymają osobnika X! Trzeba się rozejrzeć...",
    ""]

placeInfo PlayerRoom = printLines [
    "Stoisz po środku obskurnej celi.",
    "Standardowe wyposażenie: łóżko, toaleta...",
    ""]

placeInfo JailWing = printLines [
    "Znajdujesz się w ramieniu więziennym:",
    "przed tobą 4 cele - na lewo od wejścia na krześle drzemie",
    "jeden ze strażników - trzeba uważać, aby go nie wybudzić!",
    ""]

placeInfo ExitCorridor = printLines [
    "Ostatnia prosta - przed tobą stoi dwóch strażników. Stoją na tyle daleko,",
    "że was nie zauważyli, pogrążeni w rozmowie - niemniej, musisz działać szybko!",
    ""]

placeInfo Cell1 = printLines ["Nie znajdziesz tu nic ciekawego.", ""]
placeInfo Cell2 = printLines ["To cela Johnsona!", ""]
placeInfo Cell3 = placeInfo Cell1
placeInfo Cell4 = placeInfo Cell1



describePlace :: Place -> Equipment -> IO ()
describePlace Hall eq =
    if Glasses `elem` eq
        then if Extinguisher `elem` eq
            then printLines ["Wygląda na to, że nie ma tu nic ciekawego.", ""]
        else printLines [
            "Na jednej ze ścian zdaje się znajdować \ESC[31m\ESC[1mgasnica\ESC[0m - być może warto wziąć ją ze sobą...", ""]
    else printLines ["Nic nie rzuca Ci się w oczy...", ""]

describePlace BotanicComplex eq =
    if Glasses `elem` eq
        then if Key `elem` eq
            then if SpecimenX `elem` eq
                then printLines ["Na stole pośrodku sali leży sterta papierów - wygląda na jakąś specyficzną dokumentację.", ""]
            else printLines [
                "Na stole pośrodku sali leży sterta papierów - wygląda na jakąś specyficzną dokumentację.",
                "Wsród tych papierów widzisz dziwny pojemnik z napisem: \ESC[31m\ESC[1mosobnik_X\ESC[0m.", ""]
        else if SpecimenX `elem` eq
            then printLines [
                "Na stole pośrodku sali leży sterta papierów - wygląda na jakąś specyficzną dokumentację.",
                "Na szafce obok drzwi znajduje się ledwo widoczny dla Ciebie \ESC[31m\ESC[1mklucz\ESC[0m. Ciekawe które drzwi otwiera?", ""]
        else
            printLines [
                "Na stole pośrodku sali leży sterta papierów - wygląda na jakąś specyficzną dokumentację.",
                "Wsród tych papierów widzisz dziwny pojemnik z napisem: \ESC[31m\ESC[1mosobnik_X\ESC[0m.",
                "Na szafce obok drzwi znajduje się ledwo widoczny dla Ciebie \ESC[31m\ESC[1mklucz\ESC[0m. Ciekawe które drzwi otwiera?", ""]
    else printLines ["W pomieszczeniu znajduje się cała masa przeróżnych organizmów:", "od paproci i bliżej nieokreślonych grzybów aż po czerwone róże.", ""]

describePlace PlayerRoom eq =
    if Glasses `elem` eq
        then if Note `elem` eq
            then printLines ["Wygląda na to, że nie ma tu już nic ciekawego.", ""]
        else printLines ["Pod łóżkiem leży \ESC[31m\ESC[1mnotatka\ESC[0m o tym,","w której celi trzymają Johnsona - warto wziąć ją ze sobą.", ""]
    else printLines ["Na poduszce leżą twoje \ESC[31m\ESC[1mokulary\ESC[0m - szkoda byłoby je tu zostawić.", ""]

describePlace JailWing eq =
    if BunchOfKeys `elem` eq
        then printLines ["Na krześle pod ścianą siedzi drzemiący strażnik.", ""]
    else printLines [
        "Na krześle siedzi strażnik pogrążony we śnie; ma przy pasie",
        "\ESC[31m\ESC[1mpek_kluczy\ESC[0m - zdaje się, że do każdej celi.", ""]

describePlace ExitCorridor _ = printLines ["Na końcu długiego pomieszczenia znajdują się drzwi wyjściowe - nasza droga ucieczki.", ""]

describePlace Cell1 _ = printLines ["Cela jak cela, co tu dużo mówić!", ""]
describePlace Cell2 eq =
    if Johnson `elem` eq
        then printLines ["Wcześniej był tu twój partner, ale", "teraz jest to już cela jak każda inna.", ""]
    else printLines ["To on! Johnson jest tu! Uwolnij go i razem stąd ucieknijcie!", ""]
describePlace Cell3 eq = describePlace Cell1 eq
describePlace Cell4 eq = describePlace Cell1 eq



takeItem :: Place -> Equipment -> String -> (Equipment, [String])
takeItem Hall eq "gasnica" = if Extinguisher `elem` eq
        then (eq, ["Posiadasz już gaśnicę.", ""])
    else (Extinguisher : eq, ["Podnosisz gaśnicę.", ""])

takeItem BotanicComplex eq "klucz" = if Key `elem` eq
        then (eq, ["Posiadasz już klucz.", ""])
    else (Key : eq, ["Podnosisz klucz.", ""])

takeItem BotanicComplex eq "osobnik_X" = if SpecimenX `elem` eq
        then (eq, ["Posiadasz już pojemnik z osobnikiem X.", ""])
    else (SpecimenX : eq, ["Podnosisz pojemnik z osobnikiem X.", ""])

takeItem PlayerRoom eq "okulary" = if Glasses `elem` eq
        then (eq, ["Posiadasz już okulary.", ""])
    else (Glasses : eq, ["Zakładasz okulary.", ""])

takeItem PlayerRoom eq "notatka" = if Note `elem` eq
        then (eq, ["Zabrałeś już notatkę.", ""])
    else (Note : eq, ["Zabierasz notatkę.", ""])

takeItem JailWing eq "pek_kluczy" = if BunchOfKeys `elem` eq
        then (eq, ["Zabrałeś już pęk kluczy.", ""])
    else (BunchOfKeys : eq, ["Zabierasz pęk kluczy.", ""])

takeItem _ eq _ = (eq, ["W pomieszczeniu nie znajduje się taki przedmiot.", ""])


go :: Place -> Equipment -> Maybe Direction -> (Place, Maybe [String])
go Hall _ (Just N) = (ExitCorridor, Nothing)
go Hall _ (Just S) = (PlayerRoom, Nothing)
go Hall _ (Just W) = (BotanicComplex, Nothing)
go Hall eq (Just E) = if Key `elem` eq
        then (JailWing, Nothing)
    else (Hall, Just ["Drzwi zamknięte. Gdzie jest klucz do ich otwarcia?", ""])

go BotanicComplex _ (Just E) = (Hall, Nothing)

go PlayerRoom _ (Just N) = (Hall, Nothing)

go JailWing _ (Just W) = (Hall, Nothing)

go ExitCorridor _ (Just S) = (Hall, Nothing)

go Cell1 _ (Just W) = (JailWing, Nothing)
go Cell2 _ (Just W) = (JailWing, Nothing)
go Cell3 _ (Just W) = (JailWing, Nothing)
go Cell4 _ (Just W) = (JailWing, Nothing)

go place _ Nothing = (place, Just ["Zły kierunek", ""])
go place _ _ = (place, Just ["Brak przejścia w tym kierunku.", ""])



enter :: Place -> Equipment -> String -> (Place, Maybe [String])
enter JailWing eq cellName = if BunchOfKeys `elem` eq
            then case cellName of
                "cela_1" -> (Cell1, Nothing)
                "cela_2" -> (Cell2, Nothing)
                "cela_3" -> (Cell3, Nothing)
                "cela_4" -> (Cell4, Nothing)
                _        -> (JailWing, Just ["Zły numer celi.", ""])
        else (JailWing, Just ["Drzwi zamknięte. Gdzie jest klucz do ich otwarcia?", ""])

enter place _ _ = (place, Just ["Nie ma stąd przejścia do cel.", ""])


itemToString :: Item -> String
itemToString item = case item of
    Glasses      -> "okulary"
    Extinguisher -> "gaśnica"
    Key          -> "klucz"
    SpecimenX    -> "pojemnik osobnik_X"
    Note         -> "notatka"
    BunchOfKeys  -> "pęk kluczy"
    Johnson      -> "współwięzień Johnson"


printEquipment :: Equipment -> IO()
printEquipment eq = printLines eqToString where
    eqToString = [itemToString x | x <- eq] ++ [""]


readNote :: Equipment -> IO()
readNote eq = if Note `elem` eq
            then printLines [
                "┌────────────────────┐",
                "│ Johnson [REDACTED] │",
                "│                    │",
                "│ skrzydło więzienne │",
                "│      cela nr \ESC[31m\ESC[1m2\ESC[0m     │",
                "└────────────────────┘",
                ""]
        else printLines ["Nie masz żadnej notatki do odczytania.", ""]


release :: Place -> Equipment -> (Equipment, [String])
release Cell2 eq = if Johnson `elem` eq
            then (eq, ["Johnson jest już wolny.", ""])
        else (Johnson : eq, ["Uwolniłeś Johnsona! Uciekajcie czym prędzej!", ""])
release _ eq = (eq, ["Nikogo do uwolnienia tu nie ma.", ""])



attack :: Place -> Equipment -> (Bool, Bool, [String])
attack ExitCorridor eq = if Johnson `elem` eq
            then if Extinguisher `elem` eq
                then (True, False, ["W grupie siła! Walcząc ramię w ramię z Johnsonem udaje się wam pokonać stażników!", "Droga wolna! Uciekajcie, byle szybko!", ""])
            else (False, True, ["Atak bez odpowiedniej broni jest bez sensu. Strażnicy z łatwością Cię pokonują.", ""])
        else (False, True, ["Niestety - strażnicy Cię zauważyli, a działając w pojedynke nie udało Ci się ich pokonać.", ""])

attack _ _ = (False, False, ["W tym pomieszczeniu nie ma strażników do zaatakowania.", ""])



escape :: Place -> Equipment -> Bool -> [String]
escape ExitCorridor eq True = if SpecimenX `elem` eq
            then ["Dobra robota! Strażnicy pokonani, osobnik wykradziony, wy na wolności - gratulacje!", ""]
        else ["Wszystko prawie zgodnie z planem! Strażnicy pokonani, wy na wolności - tylko co", "z osobnikiem X? Został w środku! Spróbuj go wykraść!", ""]

escape ExitCorridor _ False = ["Twoje plany ucieczki spełzły na niczym.", ""]
escape _ _ _ = ["Z tego pomieszczenia nie ma wyjścia!", ""]
